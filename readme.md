# WP-TAGMANAGER

## Plugin Name
Plugin Name: Google Tag Manager
Plugin URI: http://debestekoffie.nl
Description: De beste Google Tag Manager Plugin gemaakt door De Beste Koffie.
Version: 1.0
Author: De Beste Koffie B.V.
Author URI: http://debestekoffie.nl

## Description
The Google Tag Manager plugin adds a field to the existing General Settings page for the ID and outputs the javascript in the front-end footer.

## Installation
1. Upload `google-tag-manager.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to `Settings` > `General` and set the ID from your Google Tag Manager account.